import './App.css';
import React from 'react';
import { ThemeProvider, createStyles, makeStyles, withStyles } from '@material-ui/core/styles';
import { theme } from './theme';
import Grid from '@material-ui/core/Grid';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { blue } from '@material-ui/core/colors';

const getCouncilsURL = 'http://localhost:5000/getCouncils';
const getAffairsURL = 'http://localhost:5000/getAffairs';

class App extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      sortBy: '',
      filterBy: {
        field: '',
        value: '',
      },
      councils: [],
      temp_councils: [],

      affairs: [],
      affairsSortBy: '',
      temp_affairs: [],
      affairsFilterBy: {
        field: '',
        value: '',
      },
    };
  }

  componentDidMount() {
    this.fetchCouncils();
    this.fetchAffairs();
  }

  fetchCouncils() {
    fetch(getCouncilsURL, {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json'
        },
      })
      .then(response => response.json())
      .then(resp => {
        this.setState({
          councils: resp,
          temp_councils: resp,
        });
      });
  }

  fetchAffairs() {
    fetch(getAffairsURL, {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json'
        },
      })
      .then(response => response.json())
      .then(resp => {
        this.setState({
          affairs: resp,
          temp_affairs: resp,
        });
      });
  }

  applyCouncilsFilters () {
    let _data = this.state.councils;
    const { sortBy, filterBy } = this.state;

    if (['id', 'name'].includes(sortBy)) {
      _data = this.state.councils.sort((a, b) => {
        if (a[sortBy] < b[sortBy]) return -1;
        return a[sortBy] > b[sortBy] ? 1 : 0;
      });
    }

    if (filterBy && filterBy.field && filterBy.value) {
      if (filterBy.field === 'name') {
        _data = this.state.councils.filter(el => el[filterBy.field].toLowerCase() === filterBy.value.toLowerCase());
      }
    }

    this.setState({ councils: _data });
  }

  applyAffairsFilters () {
    let _data = this.state.affairs;
    const { affairsSortBy, affairsFilterBy } = this.state;

    if (affairsSortBy === 'updated') {
      _data = this.state.affairs.sort((a, b) => {
        if (a[affairsSortBy] < b[affairsSortBy]) return -1;
        return a[affairsSortBy] > b[affairsSortBy] ? 1 : 0;
      });
    }

    if (affairsFilterBy && affairsFilterBy.field && affairsFilterBy.value) {
      if (affairsFilterBy.field === 'updated') {
        _data = this.state.affairs.filter(el => el[affairsFilterBy.field].toLowerCase() === affairsFilterBy.value.toLowerCase());
      }
    }

    this.setState({ affairs: _data });
  }

  handleSortByChange(e) {
    this.setState({ sortBy: e.target.value }, () => {
      this.applyCouncilsFilters();
    });
  }

  handleFilterByChange(e) {
    this.setState({ filterBy: { field: e.target.value, value: ''}, councils: this.state.temp_councils });
  }

  handleFilterByInputChange(e) {
    if (e.target.value === '') {
      this.setState({
        filterBy: { field: this.state.filterBy.field, value: e.target.value},
        councils: this.state.temp_councils
      });
    } else {
      this.setState({ filterBy: { field: this.state.filterBy.field, value: e.target.value} });
    }
  }

  handleClick() {
    this.applyCouncilsFilters();
  }

  handleAffairsFilterByInputChange(e) {
    if (e.target.value === '') {
      this.setState({
        affairsFilterBy: { field: this.state.affairsFilterBy.field, value: e.target.value},
        affairs: this.state.temp_affairs
      });
    } else {
      this.setState({ affairsFilterBy: { field: this.state.affairsFilterBy.field, value: e.target.value} });
    }
  }

  handleAffairsFilterByChange(e) {
    this.setState({ affairsFilterBy: { field: e.target.value, value: ''}, affairs: this.state.temp_affairs });
  }

  handleAffairsSortByChange(e) {
    this.setState({ affairsSortBy: e.target.value }, () => {
      this.applyAffairsFilters();
    });
  }

  handleAffairClick() {
    this.applyAffairsFilters();
  }

  renderCouncils() {
    const { classes } = this.props;

    return (
      <Grid item xs={12} md={6}>
        <h2>Councils Section</h2>
        <div>
          <FormControl variant="filled" className={classes.formControl} size="medium">
            <InputLabel id="demo-simple-select-filled-label">Sort Desc By</InputLabel>
            <Select
              labelId="select-council-label"
              id="select-council"
              value={this.state.sortBy}
              onChange={this.handleSortByChange.bind(this)}
            >
              <MenuItem value=""></MenuItem>
              <MenuItem value="name">Name</MenuItem>
            </Select>
          </FormControl>
          <FormControl variant="filled" className={classes.formControl}>
            <InputLabel id="demo-simple-select-filled-label">Filter By</InputLabel>
            <Select
              labelId="select-council-label"
              id="select-council"
              value={this.state.filterBy.field}
              onChange={this.handleFilterByChange.bind(this)}
            >
              <MenuItem value=""></MenuItem>
              <MenuItem value="name">Name</MenuItem>
            </Select>
          </FormControl>
          <Input type="text" value={this.state.filterBy.value} onChange={this.handleFilterByInputChange.bind(this)}></Input>
          <Button onClick={this.handleClick.bind(this)}>Search</Button>
        </div>

        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="right">Id</TableCell>
                <TableCell>Name</TableCell>
                <TableCell>Code</TableCell>
                <TableCell>Type</TableCell>
                <TableCell>Abbreviation</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.councils.map((row) => (
                <TableRow key={row.id}>
                  <TableCell align="right">{row.id}</TableCell>
                  <TableCell>{row.name}</TableCell>
                  <TableCell>{row.code}</TableCell>
                  <TableCell>{row.type}</TableCell>
                  <TableCell>{row.abbreviation}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    )
  }

  renderAffairs() {
    const { classes } = this.props;

    return (
      <Grid item xs={12} md={6}>
        <h2>Affairs Section</h2>
        <div>
          <FormControl variant="filled" className={classes.formControl}>
            <InputLabel id="demo-simple-select-filled-label">Sort Desc By</InputLabel>
            <Select
              labelId="select-affair-label"
              id="select-affair"
              onChange={this.handleAffairsSortByChange.bind(this)}
              value={this.state.affairsSortBy}
            >
              <MenuItem value=""></MenuItem>
              <MenuItem value="updated">Updated</MenuItem>
            </Select>
          </FormControl>
          <FormControl variant="filled" className={classes.formControl}>
            <InputLabel id="demo-simple-select-filled-label">Filter By</InputLabel>
            <Select
              labelId="select-affair-label"
              id="select-affair"
              onChange={this.handleAffairsFilterByChange.bind(this)}
              value={this.state.affairsFilterBy.field}
            >
              <MenuItem value=""></MenuItem>
              <MenuItem value="updated">Updated</MenuItem>
            </Select>
          </FormControl>
          <Input type="text" value={this.state.affairsFilterBy.value} onChange={this.handleAffairsFilterByInputChange.bind(this)}></Input>
          <Button onClick={this.handleAffairClick.bind(this)}>Search</Button>
        </div>

        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="right">Id</TableCell>
                <TableCell>Updated</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.affairs.map((row) => (
                <TableRow key={row.id}>
                  <TableCell align="right">{row.id}</TableCell>
                  <TableCell>{row.updated}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    )
  }

  renderContent() {
    const { classes } = this.props;    
    return (
      <div>
        <div bgcolor="primary.main">
          <h1>Welcome to Solution 4</h1>
        </div>
        <Grid container className={classes.root} spacing={8}>
          {this.renderCouncils()}

          {this.renderAffairs()}
        </Grid>
      </div>
    )
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        {this.renderContent()}
      </ThemeProvider>
    );
  }
}

const useStyles = makeStyles((theme) =>
  createStyles({
    container: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      paddingBottom: theme.spacing(1.5)
    },
    root: {
      display: 'flex',
      padding: 20,
      alignItems: 'center',
      backgroundColor: '#8080801c'
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 300,
    },
  }),
);

export default withStyles(useStyles)(App)