const express = require('express');
const cors = require('cors');
const fetch = require('node-fetch');

const PORT = 5000;
const app = express();

app.use(cors());
const corsOptions = {
    origin: "http://localhost:3000"
};

const councilsEndpoint = "http://ws-old.parlament.ch/councils?format=json";
const affairsEndpoint = "http://ws-old.parlament.ch/affairs?format=json";

// This function runs if the http://localhost:5000/getCouncillors endpoint
// is requested with a GET request
app.get('/getCouncils', cors(corsOptions), async (req, res) => {
    const fetchOptions = {
        method: 'GET',
        headers: {
          'Accept': 'text/json'
        }
    }
    const response = await fetch(councilsEndpoint, fetchOptions);
    const jsonResponse = await response.json();
    res.json(jsonResponse);
});

app.get('/getAffairs', cors(corsOptions), async (req, res) => {
    const fetchOptions = {
        method: 'GET',
        headers: {
          'Accept': 'text/json'
        }
    }
    const response = await fetch(affairsEndpoint, fetchOptions);
    const jsonResponse = await response.json();
    res.json(jsonResponse);
});


app.listen(PORT, () => {
    console.log(`Example app listening at http://localhost:${PORT}`);
});