var consumeApi = require(".");

test('consume api, sortBy id', () => {
  consumeApi('id');
});

test('consume api, sortBy firstName', () => {
  consumeApi('firstName');
});

test('consume api, sortBy firstName & filter by Adolf', () => {
  consumeApi('id', {field: 'firstName', value: 'Adolf'});
});