# How to run the solutions

For each case I have build several test cases, that you will find on the `index.test.js` file

## Test Cases

- consume api, sortBy id
- consume api, sortBy firstName
- consume api, sortBy firstName & filter by Adolf