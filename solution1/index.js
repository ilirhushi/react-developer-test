var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

function consumeApi(sortField, filterBy) {
  // sortField = 'id' | 'firstName', filterBy = {field: 'id' | 'firstName', value: ''}
  var Http = new XMLHttpRequest();
  var url = 'http://ws-old.parlament.ch/councillors?format=json';

  Http.open('GET', url);
  Http.setRequestHeader('Accept', 'text/json');
  Http.send();


  Http.onload = (e) => {

    var jsonResponse = JSON.parse(Http.responseText);
    console.log('jsonResponse: ', jsonResponse)

    var data = applyFilters(jsonResponse, sortField, filterBy);
    console.log('data', data);
  };
}

function applyFilters (data, sortField, filterBy) {
  var _data;
  if (['id', 'firstName'].includes(sortField)) {
    _data = data.sort((a, b) => {
      if (a[sortField] < b[sortField]) return -1;
      return a[sortField] > b[sortField] ? 1 : 0;
    });
  }

  if (filterBy && filterBy.field && filterBy.value) {
    _data = data.filter(el => el[filterBy.field] === filterBy.value);
  }

  return _data;
}

module.exports = consumeApi;
